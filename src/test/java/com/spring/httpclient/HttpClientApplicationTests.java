package com.spring.httpclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.httpclient.Exception.GitHubResponseError;
import com.spring.httpclient.Model.GitHubRepository;
import com.spring.httpclient.Service.GitHubRepositoryService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
public class HttpClientApplicationTests {

    @Autowired
    private GitHubRepositoryService gitHubRepositoryService;

    @Autowired
    private RestTemplate restTemplate;

    private ObjectMapper mapper = new ObjectMapper();
    private MockRestServiceServer mockServer;

    @Before
    public void init() {
        this.mockServer = MockRestServiceServer.createServer(restTemplate);
    }

    @Test
    public void getGitHubRepository_200() throws JsonProcessingException {
        GitHubRepository gitHubRepository = new GitHubRepository();
        gitHubRepository.setDescription("Issue tracker and wiki for Allegro WebAPI and REST API ");
        gitHubRepository.setFull_name("allegro/allegro-api");
        gitHubRepository.setClone_url("https://github.com/allegro/allegro-api.git");
        gitHubRepository.setStargazers_count(91);
        gitHubRepository.setCreated_at("2018-02-07T16:09:20Z");

        mockServer.expect(ExpectedCount.once(),
                requestTo("https://api.github.com/repos/allegro/allegro-api"))
                .andExpect(method(HttpMethod.GET))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(gitHubRepository))
                );

        ResponseEntity<?> response = gitHubRepositoryService.getGitHubRepositoryInfo("allegro", "allegro-api");

        mockServer.verify();
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        Assert.assertEquals(gitHubRepository.toString(), response.getBody().toString());
    }

    @Test
    public void getGitHubRepository_404() {
        mockServer.expect(ExpectedCount.once(),
                requestTo("https://api.github.com/repos/allegro-not-found/allegro-api-not-found"))
                .andExpect(method(HttpMethod.GET))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andRespond(withStatus(HttpStatus.NOT_FOUND)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("{\"message\": \"Not Found\"," +
                                "\"documentation_url\": \"https://developer.github.com/v3/repos/#get\"" +
                                "}")
                );

        ResponseEntity<GitHubResponseError> response =
                (ResponseEntity<GitHubResponseError>) gitHubRepositoryService.getGitHubRepositoryInfo("allegro-not-found", "allegro-api-not-found");

        mockServer.verify();
        Assert.assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        Assert.assertEquals("Not Found", response.getBody().getError());
        Assert.assertEquals(404, response.getBody().getStatus());
    }

    @Test
    public void getGitHubRepository_400() {
        mockServer.expect(ExpectedCount.once(),
                requestTo("https://api.github.com/repos/allegro-400/allegro-api-400"))
                .andExpect(method(HttpMethod.GET))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andRespond(withStatus(HttpStatus.BAD_REQUEST)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("{\"message\": \"Bad Request\"," +
                                "\"documentation_url\": \"https://developer.github.com/v3/repos/#get\"" +
                                "}")
                );

        ResponseEntity<GitHubResponseError> response =
                (ResponseEntity<GitHubResponseError>) gitHubRepositoryService.getGitHubRepositoryInfo("allegro-400", "allegro-api-400");

        mockServer.verify();
        Assert.assertEquals(response.getStatusCode(), HttpStatus.BAD_REQUEST);
        Assert.assertEquals("Bad Request", response.getBody().getError());
        Assert.assertEquals(400, response.getBody().getStatus());
    }

    @Test
    public void getGitHubRepository_500() {

        mockServer.expect(ExpectedCount.once(),
                requestTo("https://api.github.com/repos/allegro-500/allegro-api-500"))
                .andExpect(method(HttpMethod.GET))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("{\"message\": \"Internal Server Error\"," +
                                "\"documentation_url\": \"https://developer.github.com/v3/repos/#get\"" +
                                "}")
                );

        ResponseEntity<GitHubResponseError> response =
                (ResponseEntity<GitHubResponseError>) gitHubRepositoryService.getGitHubRepositoryInfo("allegro-500", "allegro-api-500");

        mockServer.verify();
        Assert.assertEquals(response.getStatusCode(), HttpStatus.INTERNAL_SERVER_ERROR);
        Assert.assertEquals("Internal Server Error", response.getBody().getError());
        Assert.assertEquals(500, response.getBody().getStatus());
    }
}

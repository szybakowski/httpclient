package com.spring.httpclient.Model;

public class GitHubRepository {

    private String description;
    private String full_name;
    private String clone_url;
    private Integer stargazers_count;
    private String created_at;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getClone_url() {
        return clone_url;
    }

    public void setClone_url(String clone_url) {
        this.clone_url = clone_url;
    }

    public Integer getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(Integer stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public String toString() {
        return "GitHubRepository{" +
                "description='" + description + '\'' +
                ", full_name='" + full_name + '\'' +
                ", clone_url='" + clone_url + '\'' +
                ", stargazers_count=" + stargazers_count +
                ", created_at='" + created_at + '\'' +
                '}';
    }
}

package com.spring.httpclient.Exception;

import org.springframework.http.HttpStatus;
import java.sql.Timestamp;
import java.util.Date;

public class GitHubResponseError {
    private Timestamp timestamp;
    private int status;
    private String error;
    private String message;
    private String path;

    public GitHubResponseError() {
    }

    public GitHubResponseError(HttpStatus status, String message, String path) {
        this.timestamp = new Timestamp(new Date().getTime());
        this.status = status.value();
        this.error = status.getReasonPhrase();
        this.message = message;
        this.path =path;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "GitHubResponseError{" +
                "timestamp=" + timestamp +
                ", status=" + status +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}

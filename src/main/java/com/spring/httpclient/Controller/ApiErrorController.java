package com.spring.httpclient.Controller;

import com.spring.httpclient.Exception.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.web.servlet.error.ErrorController;

@RestController
public class ApiErrorController implements ErrorController {

    @GetMapping("/error")
    public ResponseEntity<?> getErrorPage() {
        return new ResponseEntity<>(new ApiError("BAD REQUEST"), HttpStatus.BAD_REQUEST);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}

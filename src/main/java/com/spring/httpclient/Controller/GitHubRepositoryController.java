package com.spring.httpclient.Controller;

import com.spring.httpclient.Service.GitHubRepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/repositories")
public class GitHubRepositoryController {

    @Autowired
    private GitHubRepositoryService gitHubRepositoryService;

    @GetMapping("/{owner}/{repositoryName}")
    public ResponseEntity<?> getGitHubRepositoryInfo(@PathVariable String owner, @PathVariable String repositoryName) {
        return gitHubRepositoryService.getGitHubRepositoryInfo(owner, repositoryName);
    }
}

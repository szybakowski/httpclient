package com.spring.httpclient.Service;

import com.spring.httpclient.Exception.ApiError;
import com.spring.httpclient.Exception.GitHubResponseError;
import com.spring.httpclient.Model.GitHubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class GitHubRepositoryService {

    @Autowired
    private RestTemplate restTemplate;

    public ResponseEntity<?> getGitHubRepositoryInfo(String owner, String repositoryName) {
        String resourceUrl = "";

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            resourceUrl = "https://api.github.com/repos/" + owner + "/" + repositoryName;
            System.out.println("resource URL: " + resourceUrl);

            HttpEntity<Object> entity = new HttpEntity<Object>(headers);
            ResponseEntity<?> response = restTemplate.exchange(resourceUrl, HttpMethod.GET, entity, GitHubRepository.class);

            return new ResponseEntity<>(response.getBody(), response.getStatusCode());

        } catch (HttpClientErrorException e) {
            System.out.println("callToRestService HttpClientErrorException:" + e.getStatusCode() + e.getResponseBodyAsString());
            return new ResponseEntity<>(new GitHubResponseError(e.getStatusCode(), e.getMessage(), resourceUrl), e.getStatusCode());

        } catch (HttpStatusCodeException e) {
            System.out.println("callToRestService HttpStatusCodeException :" + e.getStatusCode() + e.getResponseBodyAsString());
            return new ResponseEntity<>(new GitHubResponseError(e.getStatusCode(), e.getMessage(), resourceUrl), e.getStatusCode());

        } catch (RestClientException e) {
            return new ResponseEntity<>(new GitHubResponseError(HttpStatus.REQUEST_TIMEOUT, e.getMessage(), resourceUrl), HttpStatus.REQUEST_TIMEOUT);

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(new ApiError(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
